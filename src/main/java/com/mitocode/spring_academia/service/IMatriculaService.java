package com.mitocode.spring_academia.service;

import com.mitocode.spring_academia.model.Matricula;

public interface IMatriculaService extends ICRUD<Matricula, String> {
}
