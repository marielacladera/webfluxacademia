package com.mitocode.spring_academia.service;

import com.mitocode.spring_academia.pagination.PageSupport;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ICRUD<T, ID> {

    Mono<T> registrar (T t);
    Flux<T> listar();
    Mono<T> listarPorId(ID id);
    Mono<T> modificar(T t);
    Mono<Void> eliminar(ID id);
    Mono<PageSupport<T>> listarPage(Pageable page);

}
