package com.mitocode.spring_academia.service;

import com.mitocode.spring_academia.model.Menu;
import reactor.core.publisher.Flux;

public interface IMenuService extends ICRUD<Menu, String> {

    Flux<Menu> obtenerMenus(String[] rol);

}
