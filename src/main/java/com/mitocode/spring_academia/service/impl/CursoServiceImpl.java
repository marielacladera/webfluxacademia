package com.mitocode.spring_academia.service.impl;

import com.mitocode.spring_academia.model.Curso;
import com.mitocode.spring_academia.repo.ICursoRepo;
import com.mitocode.spring_academia.repo.IGenericRepo;
import com.mitocode.spring_academia.service.ICursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CursoServiceImpl extends CRUDImpl<Curso, String> implements ICursoService {

    @Autowired
    private ICursoRepo repo;

    @Override
    protected IGenericRepo<Curso, String> getRepo() {
        return repo;
    }

}
