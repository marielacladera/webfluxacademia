package com.mitocode.spring_academia.service.impl;

import com.mitocode.spring_academia.model.Menu;
import com.mitocode.spring_academia.repo.IGenericRepo;
import com.mitocode.spring_academia.repo.IMenuRepo;
import com.mitocode.spring_academia.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class MenuServiceImpl extends CRUDImpl<Menu, String> implements IMenuService {

    @Autowired
    private IMenuRepo repo;

    @Override
    protected IGenericRepo<Menu, String> getRepo() {
        return repo;
    }

    @Override
    public Flux<Menu> obtenerMenus(String[] roles) {
        return repo.obtenerMenus(roles);
    }

}
