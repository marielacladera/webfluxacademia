package com.mitocode.spring_academia.service.impl;

import com.mitocode.spring_academia.model.Matricula;
import com.mitocode.spring_academia.repo.IGenericRepo;
import com.mitocode.spring_academia.repo.IMatriculaRepo;
import com.mitocode.spring_academia.service.IMatriculaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MatriculaServiceImpl extends CRUDImpl<Matricula, String> implements IMatriculaService {

    @Autowired
    private IMatriculaRepo repo;

    @Override
    protected IGenericRepo<Matricula, String> getRepo() {
        return repo;
    }

}
