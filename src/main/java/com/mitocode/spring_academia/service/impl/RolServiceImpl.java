package com.mitocode.spring_academia.service.impl;

import com.mitocode.spring_academia.model.Rol;
import com.mitocode.spring_academia.repo.IGenericRepo;
import com.mitocode.spring_academia.repo.IRolRepo;
import com.mitocode.spring_academia.service.IRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RolServiceImpl extends CRUDImpl<Rol, String> implements IRolService {

    @Autowired
    private IRolRepo repo;

    @Override
    protected IGenericRepo<Rol, String> getRepo() {
        return repo;
    }

}
