package com.mitocode.spring_academia.service.impl;

import com.mitocode.spring_academia.model.Estudiante;
import com.mitocode.spring_academia.repo.IEstudianteRepo;
import com.mitocode.spring_academia.repo.IGenericRepo;
import com.mitocode.spring_academia.service.IEstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EstudianteServiceImpl extends CRUDImpl<Estudiante, String> implements IEstudianteService {

    @Autowired
    private IEstudianteRepo repo;

    @Override
    protected IGenericRepo<Estudiante, String> getRepo() {
        return repo;
    }

    public Mono<List<Estudiante>> listarPorEdad() {
       Mono<List<Estudiante>> estudiantes = repo.findAll()
            .collectList();

       return estudiantes.map(est -> est.stream()
               .sorted((e1, e2) ->{
                   if(e1.getEdad() == e2.getEdad())
                       return e1.getNombres().compareTo(e2.getNombres());
                   else if(e1.getEdad()> e2.getEdad())
                           return 1;
                   else return -1;
               }).collect(Collectors.toList()));
    }
}
