package com.mitocode.spring_academia.service;

import com.mitocode.spring_academia.model.Curso;

public interface ICursoService extends ICRUD<Curso, String> {
}
