package com.mitocode.spring_academia.service;

import com.mitocode.spring_academia.model.Usuario;
import com.mitocode.spring_academia.security.User;
import reactor.core.publisher.Mono;

public interface IUsuarioService extends ICRUD<Usuario, String> {

    Mono<Usuario> registrarHash(Usuario usuario);
    Mono<User> buscarPorUsuario(String usuario);

}
