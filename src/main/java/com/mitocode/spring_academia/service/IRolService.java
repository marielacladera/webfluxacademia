package com.mitocode.spring_academia.service;

import com.mitocode.spring_academia.model.Rol;

public interface IRolService extends ICRUD<Rol, String> {
}
