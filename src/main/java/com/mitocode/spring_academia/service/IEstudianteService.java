package com.mitocode.spring_academia.service;

import com.mitocode.spring_academia.model.Estudiante;
import reactor.core.publisher.Mono;

import java.util.List;

public interface IEstudianteService extends ICRUD<Estudiante, String> {

   Mono<List<Estudiante>> listarPorEdad();

}
