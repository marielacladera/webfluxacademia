package com.mitocode.spring_academia.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "usuarios")
public class Usuario {

    @Id
    private String id;

    @Size(min = 3)
    private String usuario;

    @Size(min = 3)
    private String clave;

    @NotNull
    private Boolean estado;

    private List<Rol> roles;

}
