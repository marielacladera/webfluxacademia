package com.mitocode.spring_academia.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "matriculas")
public class Matricula {

    @Id
    private String id;

    @NotNull
    private Boolean estado;

    @NotNull
    private LocalDateTime fechaMatricula;
    //@DBRef
    @NotNull
    private Estudiante estudiante;

    @NotNull
    private List<Curso> cursos;

}
