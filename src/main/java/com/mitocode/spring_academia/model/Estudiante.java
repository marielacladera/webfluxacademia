package com.mitocode.spring_academia.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "estudiantes")
public class Estudiante {

    @Id
    private String id;

    @Size(min = 3)
    private String nombres;

    @Size(min = 3)
    private String apellidos;

    @Size(min = 5)
    private String dni;

    @NotNull
    @Min(0)
    private Integer edad;

}
