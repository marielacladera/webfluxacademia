package com.mitocode.spring_academia.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "menus")
public class Menu {

    @Id
    private String id;

    @Size(min = 3)
    private String nombre;

    @Size(min = 3)
    private String icono;

    @Size(min = 3)
    private String url;

    private List<String> roles;

}
