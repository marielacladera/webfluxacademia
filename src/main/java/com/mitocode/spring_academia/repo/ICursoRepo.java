package com.mitocode.spring_academia.repo;

import com.mitocode.spring_academia.model.Curso;

public interface ICursoRepo extends IGenericRepo<Curso, String>{
}
