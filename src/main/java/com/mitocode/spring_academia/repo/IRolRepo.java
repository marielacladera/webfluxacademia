package com.mitocode.spring_academia.repo;

import com.mitocode.spring_academia.model.Rol;

public interface IRolRepo extends IGenericRepo<Rol, String> {
}
