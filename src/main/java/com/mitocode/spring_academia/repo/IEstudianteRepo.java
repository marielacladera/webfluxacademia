package com.mitocode.spring_academia.repo;

import com.mitocode.spring_academia.model.Estudiante;

public interface IEstudianteRepo extends IGenericRepo<Estudiante, String> {
}
