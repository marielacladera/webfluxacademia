package com.mitocode.spring_academia.repo;

import com.mitocode.spring_academia.model.Usuario;
import reactor.core.publisher.Mono;

public interface IUsuarioRepo extends IGenericRepo<Usuario, String> {
    //SELECT * FROM USUARIO U WHERE U.USUARIO
    //{usuario : ?}
    //DeriveQueries
    Mono<Usuario> findOneByUsuario(String usuario);
}
