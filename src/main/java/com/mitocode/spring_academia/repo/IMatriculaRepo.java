package com.mitocode.spring_academia.repo;

import com.mitocode.spring_academia.model.Matricula;

public interface IMatriculaRepo extends IGenericRepo<Matricula, String> {
}
