package com.mitocode.spring_academia.controller;

import com.mitocode.spring_academia.model.Curso;
import com.mitocode.spring_academia.pagination.PageSupport;
import com.mitocode.spring_academia.service.ICursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Links;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;

import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;
import static reactor.function.TupleUtils.function;

@RestController
@RequestMapping("/cursos")
public class CursoController {

    @Autowired
    private ICursoService service;

    @GetMapping
    public Mono<ResponseEntity<Flux<Curso>>> listar() {
        Flux<Curso> fxCursos = service.listar();
        return Mono.just(ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(fxCursos));
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Curso>> listarPorId(@PathVariable("id") String id) {
        return service.listarPorId(id)
                .map(c -> ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                        .body(c))
                .defaultIfEmpty(new ResponseEntity<Curso>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public Mono<ResponseEntity<Curso>> registrar(@Valid @RequestBody Curso curso, final ServerHttpRequest req) {
        return service.registrar(curso)
                .map(c ->  ResponseEntity
                .created(URI.create(req.getURI().toString().concat("/").concat(c.getId())))
                .contentType(MediaType.APPLICATION_JSON)
                .body(c));
    }

    @PutMapping("/{id}")
    public Mono<ResponseEntity<Curso>> modificar(@Valid @PathVariable("id") String id, @RequestBody Curso curso) {
        Mono<Curso> monoBD = service.listarPorId(id);
        Mono<Curso> monoBody = Mono.just(curso);
        return monoBD
                .zipWith(monoBody, (bd, cur) -> {
                    bd.setId(id);
                    bd.setNombre(cur.getNombre());
                    bd.setSigla(cur.getSigla());
                    bd.setEstado(cur.getEstado());
                    return bd;
                })
                .flatMap(service::modificar)
                .map(cur -> ResponseEntity.ok()
                            .contentType(MediaType.APPLICATION_JSON)
                            .body(cur))
                .defaultIfEmpty(new ResponseEntity<Curso>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id) {
        return service.listarPorId(id)
                .flatMap(c -> {
                    return service.eliminar(c.getId())
                            .then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
                })
                .defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/hateoas/{id}")
    public Mono<EntityModel<Curso>> listarHateoasPorId(@PathVariable("id") String id) {
        Mono<Link> link1 = linkTo(methodOn(CursoController.class).listarPorId(id)).withSelfRel().toMono();
        Mono<Link> link2 = linkTo(methodOn(CursoController.class).listarPorId(id)).withSelfRel().toMono();
        return link1
                .zipWith(link2)
                .map(function((lk1, lk2) -> Links.of(lk1, lk2)))
                .zipWith(service.listarPorId(id), (lk3, c) -> EntityModel.of(c, lk3));
    }

    @GetMapping("/pageable")
    public Mono<ResponseEntity<PageSupport<Curso>>> listarPageable(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "5") int size
    ) {
        Pageable pageRequest = PageRequest.of(page, size);
        return service.listarPage(pageRequest)
                .map(pag -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(pag))
                .defaultIfEmpty(ResponseEntity.noContent().build());
    }

}
