package com.mitocode.spring_academia.controller;

import com.mitocode.spring_academia.model.Rol;
import com.mitocode.spring_academia.pagination.PageSupport;
import com.mitocode.spring_academia.service.IRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Links;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;

import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;
import static reactor.function.TupleUtils.function;

@RestController
@RequestMapping("/roles")
public class RolController {

    @Autowired
    private IRolService service;

    @GetMapping
    public Mono<ResponseEntity<Flux<Rol>>> listar() {
        Flux<Rol> fxRoles = service.listar();
        return Mono.just(ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(fxRoles));
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Rol>> listarPorId(@PathVariable("id") String id) {
        return service.listarPorId(id)
                .map(r -> ResponseEntity
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(r));
    }

    @PostMapping
    public Mono<ResponseEntity<Rol>> registrar(@Valid @RequestBody Rol rol, final ServerHttpRequest req) {
        return service.registrar(rol)
                .map(r ->  ResponseEntity
                        .created(URI.create(req.getURI().toString().concat("/").concat(r.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(r));
    }

    @PutMapping("/{id}")
    public Mono<ResponseEntity<Rol>> modificar(@Valid @PathVariable("id") String id, @RequestBody Rol rol) {
        Mono<Rol> monoBD = service.listarPorId(id);
        Mono<Rol> monoBody = Mono.just(rol);
        return monoBD
                .zipWith(monoBody, (bd, cur) -> {
                    bd.setId(id);
                    bd.setNombre(cur.getNombre());
                    return bd;
                })
                .flatMap(service::modificar)
                .map(ro -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(ro))
                .defaultIfEmpty(new ResponseEntity<Rol>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id) {
        return service.listarPorId(id)
                .flatMap(r -> {
                    return service.eliminar(r.getId())
                            .then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
                })
                .defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/hateoas/{id}")
    public Mono<EntityModel<Rol>> listarHateoasPorId(@PathVariable("id") String id) {
        Mono<Link> link1 = linkTo(methodOn(RolController.class).listarPorId(id)).withSelfRel().toMono();
        Mono<Link> link2 = linkTo(methodOn(RolController.class).listarPorId(id)).withSelfRel().toMono();
        return link1
                .zipWith(link2)
                .map(function((lk1, lk2) -> Links.of(lk1, lk2)))
                .zipWith(service.listarPorId(id), (lk3, r) -> EntityModel.of(r, lk3));
    }

    @GetMapping("/pageable")
    public Mono<ResponseEntity<PageSupport<Rol>>> listarPageable(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "5") int size
    ) {
        Pageable pageRequest = PageRequest.of(page, size);
        return service.listarPage(pageRequest)
                .map(pag -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(pag))
                .defaultIfEmpty(ResponseEntity.noContent().build());
    }

}
