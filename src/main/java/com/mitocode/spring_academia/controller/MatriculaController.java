package com.mitocode.spring_academia.controller;

import com.mitocode.spring_academia.model.Matricula;
import com.mitocode.spring_academia.pagination.PageSupport;
import com.mitocode.spring_academia.service.IMatriculaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Links;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;

import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;
import static reactor.function.TupleUtils.function;

@RestController
@RequestMapping("/matriculas")
public class MatriculaController {

    @Autowired
    private IMatriculaService service;

    @GetMapping
    public Mono<ResponseEntity<Flux<Matricula>>> listar() {
        Flux<Matricula> fxMatriculas = service.listar();
        return Mono.just(ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(fxMatriculas));
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Matricula>> listarPorId(@PathVariable("id") String id) {
        return service.listarPorId(id)
                .map(m -> ResponseEntity
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(m));
    }

    @PostMapping
    public Mono<ResponseEntity<Matricula>> registrar(@Valid @RequestBody Matricula matricula, final ServerHttpRequest req) {
        return service.registrar(matricula)
                .map(m ->  ResponseEntity
                        .created(URI.create(req.getURI().toString().concat("/").concat(m.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(m));
    }

    @PutMapping("/{id}")
    public Mono<ResponseEntity<Matricula>> modificar(@Valid @PathVariable("id") String id, @RequestBody Matricula matricula) {
        Mono<Matricula> monoBD = service.listarPorId(id);
        Mono<Matricula> monoBody = Mono.just(matricula);
        return monoBD
                .zipWith(monoBody, (bd, mat) -> {
                    bd.setId(id);
                    bd.setFechaMatricula(mat.getFechaMatricula());
                    bd.setEstado(mat.getEstado());
                    bd.setEstudiante(mat.getEstudiante());
                    bd.setCursos(mat.getCursos());
                    return bd;
                })
                .flatMap(service::modificar)
                .map(ma -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(ma))
                .defaultIfEmpty(new ResponseEntity<Matricula>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id) {
        return service.listarPorId(id)
                .flatMap(m -> {
                    return service.eliminar(m.getId())
                            .then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
                })
                .defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/hateoas/{id}")
    public Mono<EntityModel<Matricula>> listarHateoasPorId(@PathVariable("id") String id) {
        Mono<Link> link1 = linkTo(methodOn(MatriculaController.class).listarPorId(id)).withSelfRel().toMono();
        Mono<Link> link2 = linkTo(methodOn(MatriculaController.class).listarPorId(id)).withSelfRel().toMono();
        return link1
                .zipWith(link2)
                .map(function((lk1, lk2) -> Links.of(lk1, lk2)))
                .zipWith(service.listarPorId(id), (lk3, m) -> EntityModel.of(m, lk3));
    }

    @GetMapping("/pageable")
    public Mono<ResponseEntity<PageSupport<Matricula>>> listarPageable(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "5") int size
    ) {
        Pageable pageRequest = PageRequest.of(page, size);
        return service.listarPage(pageRequest)
                .map(pag -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(pag))
                .defaultIfEmpty(ResponseEntity.noContent().build());
    }
}
