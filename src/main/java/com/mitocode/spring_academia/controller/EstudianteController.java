package com.mitocode.spring_academia.controller;

import com.mitocode.spring_academia.model.Estudiante;
import com.mitocode.spring_academia.pagination.PageSupport;
import com.mitocode.spring_academia.service.IEstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Links;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;
import static reactor.function.TupleUtils.function;

@RestController
@RequestMapping("/estudiantes")
public class EstudianteController {

    @Autowired
    private IEstudianteService service;

    @GetMapping
    public Mono<ResponseEntity<Flux<Estudiante>>> listar() {
        Flux<Estudiante> fxEstudiantes = service.listar();
        return Mono.just(ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(fxEstudiantes));
    }

    @GetMapping("/edad")
    public Mono<ResponseEntity<List<Estudiante>>> listarPorEdad() {
        return service.listarPorEdad()
                .map(e -> ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(e));
    }


    @GetMapping("/{id}")
    public Mono<ResponseEntity<Estudiante>> listarPorId (@PathVariable("id") String id) {
        return service.listarPorId(id)
                .map(e -> ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(e));
    }

    @PostMapping
    public Mono<ResponseEntity<Estudiante>> registrar (@Valid @RequestBody Estudiante estudiante, final ServerHttpRequest req) {
        return service.registrar(estudiante)
                .map(e -> ResponseEntity
                .created(URI.create(req.getURI().toString().concat("/").concat(e.getId())))
                .contentType(MediaType.APPLICATION_JSON)
                .body(e));
    }

    @PutMapping("/{id}")
    public Mono<ResponseEntity<Estudiante>> modificar (@Valid @PathVariable("id") String id, @RequestBody Estudiante estudiante) {
        Mono<Estudiante> monoBody = Mono.just(estudiante);
        Mono<Estudiante> monoBD = service.listarPorId(id);
        return monoBD
                .zipWith(monoBody, (bd, est) -> {
                    bd.setId(id);
                    bd.setNombres(est.getNombres());
                    bd.setApellidos(est.getApellidos());
                    bd.setDni(est.getDni());
                    bd.setEdad(est.getEdad());
                    return bd;
                })
                .flatMap(service::modificar)
                .map(est -> ResponseEntity.ok()
                            .contentType(MediaType.APPLICATION_JSON)
                            .body(est))
                .defaultIfEmpty(new ResponseEntity<Estudiante>(HttpStatus.NOT_FOUND));

    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> eliminar (@PathVariable("id") String id) {
        return service.listarPorId(id)
               .flatMap(e -> {
                   return service.eliminar(e.getId())
                          .then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
               })
               .defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/hateoas/{id}")
    public Mono<EntityModel<Estudiante>> listarHateoasPorId(@PathVariable("id") String id) {
        Mono<Link> link1 = linkTo(methodOn(EstudianteController.class).listarPorId(id)).withSelfRel().toMono();
        Mono<Link> link2 = linkTo(methodOn(EstudianteController.class).listarPorId(id)).withSelfRel().toMono();
        /*return service.listarPorId(id)
                .zipWith(link1, (e, l) -> EntityModel.of(e,l));*/
        return link1
                .zipWith(link2)
                .map(function((lk1, lk2) -> Links.of(lk1, lk2)))
                .zipWith(service.listarPorId(id), (lk3, e) -> EntityModel.of(e, lk3));
    }

    @GetMapping("/pageable")
    public Mono<ResponseEntity<PageSupport<Estudiante>>> listarPageable(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "5") int size
    ) {
        Pageable pageRequest = PageRequest.of(page, size);
        return service.listarPage(pageRequest)
                .map(pag -> ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(pag))
                .defaultIfEmpty(ResponseEntity.noContent().build());
    }

}
