package com.mitocode.spring_academia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAcademiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAcademiaApplication.class, args);
	}

}
